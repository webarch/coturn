# Ansible role for coTURN for Nextcloud Talk on Debian

This repo contains an Ansible Galaxy role to install a
[coTURN](https://github.com/coturn/coturn) TURN and STUN server for VoIP for
[Nextcloud Talk](https://nextcloud.com/talk/) on Debian Buster (which comes
with [coTURN 4.5.1.1-1.1](https://packages.debian.org/buster/coturn)) based on
the [Nextcloud Talk
docs](https://nextcloud-talk.readthedocs.io/en/latest/TURN/), see [the
background](https://nextcloud-talk.readthedocs.io/en/latest/TURN/#background)
for an explanation for the purpose of the service.

This role generates a `/root/.coturn.auth-secret` file that is then used in the
`/etc/turnserver.conf` configuration file and the [Nextcloud
role](https://git.coop/webarch/nextcloud) also reads this file in order to
configure Nextcloud Talk.

Note that Nextcloud Talk isn't suitable for more that 3 or so users at once,
see [this thread](https://help.nextcloud.com/t/52798/22).
